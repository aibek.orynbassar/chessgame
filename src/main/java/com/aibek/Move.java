package com.aibek;

import com.aibek.figures.Figure;

public class Move {
    public Player player;
    public Spot start;
    public Spot end;
    public Figure figureMoved;
}
