package com.aibek;

import com.aibek.figures.Figure;

public class Spot {
    private int x;
    private int y;

    private Figure figure;
    private Spot(int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setFigure(Figure figure) {
        this.figure = figure;
    }
    public boolean hasFigure(){
        return false;
    }
}
