package com.aibek;

public enum GameStatus {
    ACTIVE,
    DRAW,
    WHITEWIN,
    VLACKWIN
}
