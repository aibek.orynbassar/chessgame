package com.aibek;

import java.util.List;

public class Game {
    private GameStatus status;
    private Board board;
    private Player[] players = new Player[2];
    private List<Move> movesPlayed;
    private int playerCurrentWeight;

    public void startGame(){}
    public boolean PlayerMove(Player player){
        return player.getPlayerTurn();
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public int getPlayerCurrentWeight() {
        return playerCurrentWeight;
    }
}
