package com.aibek.figures;

import com.aibek.Color;

public class Queen extends AbstractFigure{
    public Queen(Color color){
        super(color);
    }
    @Override
    public boolean moveValid() {
        return false;
    }
    @Override
    public int getWeight() {
        return 0;
    }
}
