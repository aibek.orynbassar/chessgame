package com.aibek.figures;

import com.aibek.Color;

public interface Figure {
    Color getColor();

    int getWeight();
    boolean moveValid();
    boolean isAlive();
    void setAlive(boolean isAlive);
}
