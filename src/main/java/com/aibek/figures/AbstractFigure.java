package com.aibek.figures;

import com.aibek.Color;

public abstract class AbstractFigure implements Figure{
    private boolean isAlive;
    private final Color color;

    public AbstractFigure(Color color){
        this.color = color;
    }
    public Color getColor(){
        return null;
    }

    public abstract int getWeight();
    public abstract boolean moveValid();
    public boolean isAlive(){
        return isAlive;
    }
    public void setAlive(boolean isAlive){
        this.isAlive = isAlive;
    }
}
