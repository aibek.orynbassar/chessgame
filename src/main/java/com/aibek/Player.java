package com.aibek;

public class Player {
    private String name;
    private Color colorSide;
    private boolean playerTurn;
    public void setName(String name){
        this.name = name;
    }
    public String getName(){
        return name;
    }

    public void setColorSide(Color colorSide){
        this.colorSide = colorSide;
    }
    public Color getColorSide(){
        return colorSide;
    }


    public void setPlayerTurn(boolean playerTurn){
        this.playerTurn = playerTurn;
    }
    public boolean getPlayerTurn(){
        return playerTurn;
    }
}
